package ru.job4j.chess;

/***
 * @author inna-timonova89 (ms.timonovai@mail.ru)
 */

public class ObjectNotFoundException extends RuntimeException {
    public ObjectNotFoundException(String rsn) {
        super(rsn);
    }
}
