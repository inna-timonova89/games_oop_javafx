package ru.job4j.chess;

/***
 * @author inna-timonova89 (ms.timonovai@mail.ru)
 */

public class OccupiedPositionException extends RuntimeException {
    public OccupiedPositionException(String rsn) {
        super(rsn);
    }
}
