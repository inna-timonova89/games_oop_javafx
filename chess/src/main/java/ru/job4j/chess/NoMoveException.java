package ru.job4j.chess;

/***
 * @author inna-timonova89 (ms.timonovai@mail.ru)
 */
public class NoMoveException extends RuntimeException {
    public NoMoveException(String rsn) {
        super(rsn);
    }
}
